include::partial$entities.adoc[]

= Release Notes

Release Notes for Fedora Linux {PRODVER}

[abstract]
--
This document provides the release notes for Fedora Linux {PRODVER}. It describes major changes offered as compared to Fedora Linux {PREVVER}.
// Fedora Linux {PRODVER} was released on YYYY-MM-DD.
--

[TIP]
====
Have something that should be included in the release notes?
Notice something wrong?
File an issue in the https://gitlab.com/fedora/docs/fedora-linux-documentation/release-notes/-/issues[Release Notes repository].
====

[NOTE]
====
Use the sidebar on the left to navigate the Release Notes as well as other documentation for Fedora {PRODVER}.
====

image:title_logo.svg[Fedora Logo]
include::partial$Legal_Notice.adoc[]
